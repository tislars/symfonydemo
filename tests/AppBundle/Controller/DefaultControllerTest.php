<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }

    public function testNumber()
    {
    	$client = static::createClient();

        $crawler = $client->request('GET', '/number');
        $number = $crawler->filter('#number')->text();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $number);
        $this->assertLessThan(100, $number);

        if ($number < 50)
        {
        	$this->assertGreaterThan(0, $number);
        } else {
        	$this->assertLessThan(100, $number);
        }
    }
}
